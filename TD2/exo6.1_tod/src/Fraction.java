
public class Fraction {
	private int numerateur;
	private int denominateur;
	
	public Fraction (int numerateur, int denominateur) {
		this.numerateur=numerateur;
		this.denominateur=denominateur;
	}
	
	public Fraction (Constante c1) {
		if(c1==Constante.ZERO) {
			this.numerateur=0;
			this.denominateur=1;
		}
		if(c1==Constante.UN) {
			this.numerateur=1;
			this.denominateur=1;
		}
	}
	
	public Fraction (int numerateur) {
		this.numerateur=numerateur;
		this.denominateur=1;
	}
	
	public Fraction () {
		this.numerateur=0;
		this.denominateur=1;
	}
	
	@Override
	public String toString() {
		return "("+this.numerateur+"/"+this.denominateur+")";
	}
	
	public double getDouble() {
		if(denominateur==0) return -1;
		else {
			return (double)this.numerateur/this.denominateur;
		}
	}
	
	public int getNumerateur() {
		return this.numerateur;
	}
	
	public int getDenominateur() {
		return this.denominateur;
	}
	
	
	
}
